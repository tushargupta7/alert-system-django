from django.conf.urls import url
from . import views

app_name = 'api'

urlpatterns = [
    #url(r'^$', views.index, name='index'),
    url(r'^upload/$', views.uploadImage, name='upload'),
    #url(r'^login_user/$', views.login_user, name='login_user'),
    #url(r'^logout_user/$', views.logout_user, name='logout_user'),
    url(r'^alert/(?P<user_id>[0-9]+)/$', views.alert, name='alert'),
    url(r'^updateToken/(?P<user_id>[0-9]+)/$', views.updateToken, name='token'),
    url(r'^status/(?P<device_hash>[A-Z][0-9]+)/$', views.getStatus, name='status'),
    url(r'^getAlertImage/(?P<user_id>[0-9]+)/$', views.getAlertImage, name='image'),
    url(r'^action/(?P<user_id>[0-9]+)/$', views.userAction, name='action'),
    url(r'^getaction/(?P<device_id>[A-Z][0-9]+)/$', views.getAction, name='getaction'),
    #url(r'^(?P<song_id>[0-9]+)/favorite/$', views.favorite, name='favorite'),
    #url(r'^songs/(?P<filter_by>[a-zA_Z]+)/$', views.songs, name='songs'),
    #url(r'^create_album/$', views.create_album, name='create_album'),
    #url(r'^(?P<album_id>[0-9]+)/create_song/$', views.create_song, name='create_song'),
    #url(r'^(?P<album_id>[0-9]+)/delete_song/(?P<song_id>[0-9]+)/$', views.delete_song, name='delete_song'),
    #url(r'^(?P<album_id>[0-9]+)/favorite_album/$', views.favorite_album, name='favorite_album'),
    #url(r'^(?P<album_id>[0-9]+)/delete_album/$', views.delete_album, name='delete_album'),
]
