import jsonpickle as jsonpickle
import numpy as np
import cv2
from PIL import Image
from django.http import JsonResponse, FileResponse, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
import json

from rest_framework.decorators import api_view
from django.core.exceptions import ObjectDoesNotExist
from .models import Users, CamDevice, States, ActionTable


def alert(request, user_id):
    user = get_object_or_404(Users, pk=user_id)
    cam_device = user.camdevice_set.get_queryset().all().first()
    try:
        if cam_device.state.name == 'ALERT':
            cam_device.state = get_object_or_404(States, pk=1)
        else:
            cam_device.state = get_object_or_404(States, pk=2)
        cam_device.save()
    except (KeyError, CamDevice.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True,
                             })

@csrf_exempt
def userAction(request, user_id):
    user = get_object_or_404(Users, pk=user_id)
    try:
        userActionJson = json.loads(request.body.decode('utf -8'))
        userAction = userActionJson['action']
        action = ActionTable.objects.create()
        action.action = userAction
        action.message = userActionJson['message']
        action.device_id = user.camdevice_set.first().endpoint_hash
        action.save()
        cam_device=user.camdevice_set.first()
        cam_device.state = get_object_or_404(States, pk=2)
        cam_device.save()
    except ObjectDoesNotExist:
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True,
                             })


def getStatus(request, device_hash):
    try:
        cam = get_object_or_404(CamDevice, endpoint_hash=device_hash)
        status = cam.state.name
    except (KeyError, CamDevice.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True, 'state': status})


@csrf_exempt
def updateToken(request, user_id):
    user = get_object_or_404(Users, pke=user_id)
    user_token = json.loads(request.body.decode('utf -8'))
    try:
        user.access_token = user_token['token']
        user.save()
    except (KeyError, CamDevice.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})


@api_view(('GET',))
@csrf_exempt
def getAlertImage(request, user_id):
    user = get_object_or_404(Users, pk=user_id)
    deviceCam = user.camdevice_set.first().endpoint_hash
    response = HttpResponse()
    response['content-type'] = 'image/jpeg'
    # response.has_header(header=header)
    imagePath = './images/' + deviceCam + '.jpg'
    img = Image.open(imagePath)
    img.save(response, 'jpeg')
    return response


@csrf_exempt
def uploadImage(request):
    # convert string of image data to uint8
    # jsonLoad=json.loads(request.body.decode('utf-8'))
    imageData = request.body

    nparr = np.fromstring(imageData, np.uint8)
    # decode image
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    directoryPath = './images/'
    imageFileName = 'R1'
    cv2.imwrite(directoryPath + imageFileName + '.jpg', img)

    # do some fancy processing here....

    # build a response dict to send back to client
    response = {'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0])
                }
    # encode response using jsonpickle
    response_pickled = jsonpickle.encode(response)

    return JsonResponse({'status': 200, 'message': 'image received. size={}x{}'.format(img.shape[1], img.shape[0])})


def removeActionandMarkSafe(device_id):
    get_object_or_404(ActionTable,device_id=device_id).delete()
    device=get_object_or_404(CamDevice, endpoint_hash=device_id)
    device.state=get_object_or_404(States, pk=1)
    device.save()




def getAction(request, device_id):
    try:
        device=get_object_or_404(CamDevice, endpoint_hash=device_id)
        if device.state.name == 'ALERT':
            device_action = get_object_or_404(ActionTable, device_id=device_id)
            response= {'success': True, 'action': device_action.action, 'message': device_action.message }
            removeActionandMarkSafe(device_action.device_id)
            return JsonResponse(response)
        else:
            return JsonResponse({'action': 'None', 'message': 'No action needed'})
    except ObjectDoesNotExist:
        return JsonResponse({'success': False})