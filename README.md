**Python Django Backend Server** that sends alerts to a Person if anyone visits the person on his desk and he was unavailable at desk.<br/>
Use case:<br/>
You are not available at desk. A peer visits desk.<br/>
Application running on Laptop/Raspberry pi detects human using AI and ends notifiction to android application with the peers photo.<br/>
You can respond to person via andorid app that will trigger voice commands from your PC/RapberryPi.<br/>
<br/><br/>
Enjoy your own Personal Assistant.
